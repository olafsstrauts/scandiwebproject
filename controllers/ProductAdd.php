<?php


class ProductAdd{

    public static function crateView ()
    {
        require "../resources/ProductAdd.html.php";

    }

    public static function insertProduct ()
    {
        $succses = "";

        $postArray = $_POST;

        $sku = isset($postArray['sku']) && !empty($postArray['sku']) ? $postArray['sku'] : null;
        $name = isset($postArray['name']) && !empty($postArray['name']) ? $postArray['name'] : null;
        $price = isset($postArray['price']) && !empty($postArray['price']) ? $postArray['price'] : null;

        $size = isset($postArray['size']) && !empty($postArray['size']) ? $postArray['size'] : null;

        $weight = isset($postArray['weight']) && !empty($postArray['weight']) ? $postArray['weight'] : null;

        $height = isset($postArray['height']) && !empty($postArray['height']) ? $postArray['height'] : null;
        $width = isset($postArray['width']) && !empty($postArray['width']) ? $postArray['width'] : null;
        $length = isset($postArray['length']) && !empty($postArray['length']) ? $postArray['length'] : null;

        if ($sku != null && $name != null && ($price != null && is_numeric($price))){

            //only if all of the values are set product is added to database and status is returned
            //otherwise error is returned
            $itemValues = [
                'sku' => $sku,
                'name' => $name,
                'price' => $price,
            ];

            if ($size != null && is_numeric($size)) {

                $itemValues['size'] = $size;
                $succses =  DvdDisc::insertItem($itemValues);

            } elseif ($weight != null && is_numeric($weight)) {

                $itemValues['weight'] = $weight;
                $succses =  Book::insertItem($itemValues);

            } elseif (($height != null && is_numeric($height))
                && ($width != null && is_numeric($width))
                && ($length != null && is_numeric($length))) {

                $itemValues['height'] = $height;
                $itemValues['width'] = $width;
                $itemValues['length'] = $length;
                $succses =  Furniture::insertItem($itemValues);

            } else $succses =  "Parameters not set correctly!";

        }

        else $succses =  "Product values not set!";

        require "../resources/ProductAdd.html.php";
















/*
        $ip = new InsertProduct();
        $succses = $ip->insert();
        require "../resources/ProductAdd.html.php";
*/
    }

}