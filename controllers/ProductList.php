<?php

class ProductList
{

    public static function createView()
    {
        require "../resources/ProductList.html.php";
    }

    public static function getProducts()
    {
        $itemsArray = "";
        if (isset($_POST))
        {
            $item = $_POST;

            switch ($item['options']) {

                case 'dvd':
                    $itemsArray = DvdDisc::findItem();
                    break;
                case 'book':
                    $itemsArray = Book::findItem();
                    break;
                case 'furniture':
                    $itemsArray = Furniture::findItem();
                    break;
            }
        } else echo "Option not selected";

        echo $itemsArray;
    }
}