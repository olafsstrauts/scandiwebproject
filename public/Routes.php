<?php
//Simple mvc router that sets routes

    Route::set('ProductAdd', function() {
        ProductAdd::crateView();
    });

    Route::set('ProductList', function() {
        ProductList::createView();
    });

    Route::set('ListProducts', function (){
        ProductList::getProducts();
    });

    Route::set('index.php', function() {
        Home::createView();
    });

    Route::set('InsertProduct', function () {
        ProductAdd::insertProduct();
    });

    Route::set('CreateTables', function () {
        $db = new CreateTables();
        $createTables =  $db->createTables();
        require "../resources/home.php";


    });

