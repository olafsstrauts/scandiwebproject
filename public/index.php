<?php

//added .htaccess file always redirects to index.php

//spl_autoload makes sure that includes dose not have to be required
spl_autoload_register(function ($class) {
    if (file_exists('../classes/' . $class . '.php')){
        require_once '../classes/' . $class . '.php';
    } elseif (file_exists('../controllers/' . $class . '.php')) {
        require_once '../controllers/' . $class . '.php';
    } elseif (file_exists('../classes/PDO/' . $class . '.php')){
        require_once '../classes/PDO/' . $class . '.php';
    }
});

require_once('Routes.php');




