/*
    ajax request for dynamically change page content  i.e. update page contents without having to reload the page.
    what function does is creates a post request calling GetProduct class which returns all values depending on the selected option,
    then function displayItems adds found information to the page

 */

$( "form" ).on( "submit", function( event ) {
    event.preventDefault();
    $.ajax ({
        type: 'post',
        data: $('form').serialize(),
        url:  $('#selectForm').attr('action'),
        dataType: 'JSON',
        success: function (data) {
            displayItems(data);
        }

    })
});

function displayItems(data){
    $("#itemsContainer").html("");

    $.each( data, function (key, value) {
        $("#itemsContainer").append(
            "<div class=\"text-center border border-dark m-2 p-2\" style=\"width: 268px;\"" +
                "<p><strong>" + value.sku_tag + "</strong>" + value.sku + "</p>" +
                "<p><strong>" + value.name_tag + "</strong>" + value.name + "</p>" +
                "<p><strong>" + value.price_tag + "</strong>" + value.price + " $</p>" +
                "<p><strong>" + value.item_tag +"</strong>" + value.item_value + value.item_value_tag + "</p>" +
            "</div>");

    })
}
