//upon selecting one of options values, page contents change allowing to input
//different values depending of selected attribute

$('#typeSwitch').change(function(){

    switch ($(this).val()) {
        case "size": {
            appendSize();
            break
        }
        case "weight": {
            appendWeight();
            break
        }
        case "hwl": {
            appendHwl();
            break
        }
        case "": {
            blancTypeSwitch();
            break
        }
    }
});

function appendSize(){
    $("#options").html("<label for=\"size\" class=\"col-sm-2 col-form-label\">Size</label>\n" +
        "                <div class=\"col-sm-10\">\n" +
        "                    <input type=\"text\" class=\"form-control\" name=\"size\">\n" +
        "                </div>");
    $("#description").html("<p>Please provide the size in mega bytes for the given DVD-disc</p>");
}

function appendWeight() {
    $("#options").html("<label for=\"weight\" class=\"col-sm-2 col-form-label\">Weight</label>\n" +
        "                <div class=\"col-sm-10\">\n" +
        "                    <input type=\"text\" class=\"form-control\" name=\"weight\">\n" +
        "                </div>");
    $("#description").html("<p>Please provide the size in kilograms for the given book</p>" );
}

function appendHwl(){
    var dimentions = {
        "height" : "Height",
        "width" : "Width",
        "length" : "Length"
    };
    $("#options").html("");
    $.each(dimentions, function(key, value){
        $("#options").append("<label for=\""+  key +"\" class=\"col-sm-2 col-form-label\">" + value +"</label>\n" +
            "                <div class=\"col-sm-10\">\n" +
            "                    <input type=\"text\" class=\"form-control\" name=\"" + key +"\">\n" +
            "                </div>");
    });
    $("#description").html("<p>Please provide the size in height, width and length in centimeters for the given furniture</p>" );
}

function blancTypeSwitch(){
    $("#options").html("");
}
