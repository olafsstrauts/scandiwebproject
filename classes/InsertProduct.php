<?php

class InsertProduct
{
    private $sku;
    private $name;
    private $price;
    private $size;
    private $weight;
    private $height;
    private $width;
    private $length;

    private $conn;

    public function __construct()
    {
        $postArray = $_POST;

        $this->sku = isset($postArray['sku']) && !empty($postArray['sku']) ? $postArray['sku'] : null;
        $this->name = isset($postArray['name']) && !empty($postArray['name']) ? $postArray['name'] : null;
        $this->price = isset($postArray['price']) && !empty($postArray['price']) ? $postArray['price'] : null;

        $this->size = isset($postArray['size']) && !empty($postArray['size']) ? $postArray['size'] : null;
        $this->weight = isset($postArray['weight']) && !empty($postArray['weight']) ? $postArray['weight'] : null;

        $this->height = isset($postArray['height']) && !empty($postArray['height']) ? $postArray['height'] : null;
        $this->width = isset($postArray['width']) && !empty($postArray['width']) ? $postArray['width'] : null;
        $this->length = isset($postArray['length']) && !empty($postArray['length']) ? $postArray['length'] : null;

    }

    public function insert ()
    {
        if ($this->sku != null && $this->name != null && ($this->price != null && is_numeric($this->price))){

            $instance = ConnectionDb::getInstance();
            $this->conn = $instance->getConnection();

            //only if all of the values are set product is added to database and status is returned
            //otherwise error is returned

            if ($this->size != null && is_numeric($this->size)) {
                return $this -> addDvdToDatabase();
            } elseif ($this->weight != null && is_numeric($this->weight)) {
                return $this -> addBookToDatabase();
            } elseif (($this->height != null && is_numeric($this->height))
                    && ($this->width != null && is_numeric($this->width))
                    && ($this->length != null && is_numeric($this->length))) {
                return $this ->addFurnitureToDatabase();
            } else return "Parameters not set correctly!";

        }

        else return "Product values not set!";
    }

    /*
         * first sku, name price is inserted in product_values table
         * then product properties value(s) are inserted
         * then retrieving both last inserted ids and using product code, entry in product table is made
         * (product table consists of all tables foreign keys)
         */


    private function addDvdToDatabase()
    {


        $dvdProductCode = 1;
        $lastProdId = $this -> insertProductValues();


        $stmt = $this->conn->prepare("INSERT INTO properties (size) VALUES (:size)");
        $stmt->bindParam(':size', $this->size);
        $stmt->execute();
        $lastPropertiesId = $this->conn->lastInsertId();

        $success = $this->insertProduct($dvdProductCode, $lastProdId, $lastPropertiesId);

        if($success)
            return 'DVD-disc added!';


    }



    private function addBookToDatabase()
    {
        $BookProductCode = 2;
        $lastProdId = $this -> insertProductValues();


        $stmt = $this->conn->prepare("INSERT INTO properties (weight) VALUES (:weight)");
        $stmt->bindParam(':weight', $this->weight);
        $stmt->execute();
        $lastPropertiesId = $this->conn->lastInsertId();

        $success = $this->insertProduct($BookProductCode, $lastProdId, $lastPropertiesId);

        if($success)
            return "Book added!";

    }

    private function addFurnitureToDatabase()
    {
        $FurnitureProductCode = 3;
        $lastProdId = $this -> insertProductValues();


        $stmt = $this->conn->prepare("INSERT INTO properties (height, width, length) VALUES (:height, :width, :length)");
        $stmt->bindParam(':height', $this->height);
        $stmt->bindParam(':width', $this->width);
        $stmt->bindParam(':length', $this->length);
        $stmt->execute();
        $lastPropertiesId = $this->conn->lastInsertId();

        $success = $this->insertProduct($FurnitureProductCode, $lastProdId, $lastPropertiesId);

        if($success)
            return "Furniture added!";

    }

    private function insertProductValues()
    {
        $stmt = $this->conn->prepare("INSERT INTO product_values (sku, name, price) VALUES (:sku, :name, :price)");
        $stmt->bindParam(':sku', $this->sku);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':price', $this->price);
        $stmt->execute();
        return $this->conn->lastInsertId();
    }

    private function insertProduct($id_item_name, $id_product_values, $id_properties)
    {
        $stmt = $this->conn->prepare("INSERT INTO products (id_item_name, id_product_values, id_properties) VALUES (:name, :value, :properties)");
        $stmt->bindParam(':name', $id_item_name);
        $stmt->bindParam(':value', $id_product_values);
        $stmt->bindParam(':properties', $id_properties);
        return $stmt->execute();

    }

}