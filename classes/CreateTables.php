<?php

class CreateTables
{
    private $conn;

    public function __construct()
    {
        //creating database instance to add tables
        $config = include("../config.php");
        $dsn = $config['database']['dsn'];
        $username = $config['database']['username'];
        $password = $config['database']['password'];
        $nameDB = $config['database']['name'];

        $this->conn = new PDO("mysql:host={$dsn}; dbname={$nameDB}", $username,$password,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

    }

    public function createTables()
    {

        $itn  = $this->createItemName();
        $prval  = $this->createProductValues();
        $prop  = $this->createProperties();
        $prod = $this->createProducts();

        // exex returns 0 if table is created successfully. if condittion is true item_name table is filed with item names - DVD, Book, Furniture
        if($itn === 0 && $prval === 0 && $prop === 0 && $prod === 0) {
            $this->fillItemName();
            return "Tables Created Successfully";
        } elseif ($this->conn->query("SELECT 1 FROM item_name LIMIT 1") == true ) {
            return "Tables already exist!";
        }
        else return "Tables not created";
    }



    private function createItemName()
    {
        $sql = "CREATE TABLE item_name (
            id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(30) NOT NULL )ENGINE=InnoDB";
        return $this->conn->exec($sql);
    }

    private function createProductValues()
    {
        $sql = "CREATE TABLE product_values (
            id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            sku VARCHAR(30) NOT NULL,
            name VARCHAR(30) NOT NULL,
            price DECIMAL(10,2) NOT NULL )ENGINE=InnoDB";
        return $this->conn->exec($sql);
    }

    private function createProperties()
    {
        $sql = "CREATE TABLE properties (
            id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            size INT NULL,
            weight INT NULL,
            height INT NULL, 
            width INT NULL,
            length INT NULL) ENGINE=INNODB";
        return $this->conn->exec($sql);
    }

    private function createProducts()
    {
        $sql = "CREATE TABLE products (
            id_item_name  INT UNSIGNED,
            id_product_values INT UNSIGNED,
            id_properties INT UNSIGNED,
            CONSTRAINT FK_ITEM_NAME FOREIGN KEY (id_item_name)
            REFERENCES item_name (id),
            CONSTRAINT FK_PRODUCT_VALUES FOREIGN KEY (id_product_values)
            REFERENCES product_values (id),
            CONSTRAINT FK_PROPERTIES FOREIGN KEY (id_properties)
            REFERENCES properties (id)
            ) ENGINE=INNODB";
        return $this->conn->exec($sql);
    }

    private function fillItemName()
    {
        $types = array('DVD', 'Book', 'Furniture');
        foreach ($types as $type) {
            $stmt = $this->conn->prepare("INSERT INTO item_name (name) VALUES (:type)");
            $stmt->bindParam(':type', $type);
            $stmt->execute();
        }

    }

}