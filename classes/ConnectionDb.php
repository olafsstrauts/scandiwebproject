<?php

    /*
    creates database connection with simpleton pattern i.e.
    just one instance can be created
    */
    class ConnectionDb
    {
        private static $instance = null;
        private $connection;

        private function __construct()
        {
            $config = include("../config.php");
            $dsn = $config['database']['dsn'];
            $username = $config['database']['username'];
            $password = $config['database']['password'];
            $nameDB = $config['database']['name'];

            $this->connection = new PDO("mysql:host={$dsn}; dbname={$nameDB}", $username,$password,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        }

        public static function getInstance()
        {
            if(!self::$instance)
                self::$instance = new ConnectionDb();

            return self::$instance;
        }

        public function getConnection()
        {
            return $this->connection;
        }

    }

