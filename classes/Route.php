<?php

/*
  if the url in the browser corresponds to one of the routes defined in the Routes.php a function is invoked
  that handles the request
 */

class Route {

    public static $validRoutes = array();

    public static function set($route, $function)
    {
        self::$validRoutes[] = $route;

        if ($_GET['url'] == $route)
            $function->__invoke();
    }
}