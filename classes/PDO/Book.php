<?php


class Book extends Item
{
    public $weight;


    public function __construct()
    {
        parent::__construct();
        $this->item_value = $this->weight;
        $this->item_tag = 'Weight: ';
        $this->item_value_tag = ' KG';
    }


    public static function findItem()
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $stmt = $conn->query("SELECT pv.sku, pv.name, pv.price, pr.weight FROM product_values AS pv
                                              INNER JOIN products AS prod ON pv.id = prod.id_product_values
                                              INNER JOIN properties AS pr ON prod.id_properties = pr.id
                                              WHERE prod.id_item_name = 2");
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Book');
        $bookArray = $stmt->fetchAll();
        return json_encode($bookArray);
    }

    public static function insertItem($item)
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $BookProductCode = 2;
        $lastProdId = self::insertProductValues($item['sku'], $item['name'], $item['price']);


        $stmt = $conn->prepare("INSERT INTO properties (weight) VALUES (:weight)");
        $stmt->bindParam(':weight', $item['weight']);
        $stmt->execute();
        $lastPropertiesId = $conn->lastInsertId();

        $success = self::insertProduct($BookProductCode, $lastProdId, $lastPropertiesId);

        if($success)
            return "Book added!";
    }

}