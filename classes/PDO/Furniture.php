<?php


class Furniture extends Item
{
    public $height;
    public $width;
    public $length;


    public function __construct()
    {
        parent::__construct();
        $this->item_value = $this->height.'x'.$this->width.'x'.$this->length;
        $this->item_tag = 'Dimensions: ';
        $this->item_value_tag = ' cm';
    }

    public static function findItem()
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $stmt = $conn->query("SELECT pv.sku, pv.name, pv.price, pr.height, pr.width, pr.length FROM product_values AS pv
                                              INNER JOIN products AS prod ON pv.id = prod.id_product_values
                                              INNER JOIN properties AS pr ON prod.id_properties = pr.id
                                              WHERE prod.id_item_name = 3");
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Furniture');
        $furnitureArray = $stmt->fetchAll();
        return json_encode($furnitureArray);
    }

    public static function insertItem($item)
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $FurnitureProductCode = 3;
        $lastProdId = self::insertProductValues($item['sku'], $item['name'], $item['price']);


        $stmt = $conn->prepare("INSERT INTO properties (height, width, length) VALUES (:height, :width, :length)");
        $stmt->bindParam(':height', $item['height']);
        $stmt->bindParam(':width', $item['width']);
        $stmt->bindParam(':length', $item['length']);
        $stmt->execute();
        $lastPropertiesId = $conn->lastInsertId();

        $success = self::insertProduct($FurnitureProductCode, $lastProdId, $lastPropertiesId);

        if($success)
            return "Furniture added!";
    }

}