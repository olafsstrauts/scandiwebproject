<?php


class DvdDisc extends Item
{
    public $size;

    public function __construct()
    {
        parent::__construct();
        $this->item_value = $this->size;
        $this->item_tag = 'Size: ';
        $this->item_value_tag = ' MB';
    }



    public static function findItem()
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $stmt = $conn->query("SELECT pv.sku, pv.name, pv.price, pr.size FROM product_values AS pv
                                              INNER JOIN products AS prod ON pv.id = prod.id_product_values
                                              INNER JOIN properties AS pr ON prod.id_properties = pr.id
                                              WHERE prod.id_item_name = 1");
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'DvdDisc');
        $dvdArray = $stmt->fetchAll();
        return json_encode($dvdArray);

    }


    public static function insertItem($item)
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $dvdProductCode = 1;
        $lastProdId = self::insertProductValues($item['sku'], $item['name'], $item['price']);


        $stmt = $conn->prepare("INSERT INTO properties (size) VALUES (:size)");
        $stmt->bindParam(':size', $item['size']);
        $stmt->execute();
        $lastPropertiesId = $conn->lastInsertId();

        $success = self::insertProduct($dvdProductCode, $lastProdId, $lastPropertiesId);

        if($success)
            return 'DVD-disc added!';

    }

}