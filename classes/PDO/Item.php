<?php

/*
 * all the logic concerning product listing and adding is moved to the Item abstract class
 * and its subclasses, each for every item.
 *
 * all the derived classes inherits findItem and insertItem methods as well as can access static methods
 * insertProductValues and insertProducts inside Items class
 *
 */


abstract class Item
{
    public $sku;
    public $name;
    public $price;
    public $item_value;

    public $sku_tag;
    public $name_tag;
    public $price_tag;
    public $item_tag;
    public $item_value_tag;


    public function __construct()
    {
        $this->sku_tag = 'SKU: ';
        $this->name_tag = 'Name: ';
        $this->price_tag = 'Price: ';
    }


    abstract public static function findItem();
    abstract public static function insertItem($item);

    protected static function insertProductValues($sku, $name, $price)
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $stmt = $conn->prepare("INSERT INTO product_values (sku, name, price) VALUES (:sku, :name, :price)");
        $stmt->bindParam(':sku', $sku);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':price', $price);
        $stmt->execute();
        return $conn->lastInsertId();
    }

    protected static function insertProduct($id_item_name, $id_product_values, $id_properties)
    {
        $instance = ConnectionDb::getInstance();
        $conn = $instance->getConnection();

        $stmt = $conn->prepare("INSERT INTO products (id_item_name, id_product_values, id_properties) VALUES (:name, :value, :properties)");
        $stmt->bindParam(':name', $id_item_name);
        $stmt->bindParam(':value', $id_product_values);
        $stmt->bindParam(':properties', $id_properties);
        return $stmt->execute();

    }

}