<?php

class GetProduct
{
    private $conn;

    public function __construct()
    {
        $instance = ConnectionDb::getInstance();
        $this->conn = $instance->getConnection();
        $post = $_POST;

        switch ($post['options']) {

            case 'dvd': $this->findDvds();
            break;
            case 'book': $this->findBooks();
            break;
            case 'furniture': $this->findFurniture();
            break;
        }
    }
    /*
     * a query is executed that retrieves all the values depending on the product type
     *
     */

    private function findDvds()
    {
        $stmt = $this->conn->query("SELECT pv.sku, pv.name, pv.price, pr.size FROM product_values AS pv
                                              INNER JOIN products AS prod ON pv.id = prod.id_product_values
                                              INNER JOIN properties AS pr ON prod.id_properties = pr.id
                                              WHERE prod.id_item_name = 1");
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'DvdDisc');
        $dvdArray = $stmt->fetchAll();
        echo json_encode($dvdArray);
    }

    private function findBooks()
    {
        $stmt = $this->conn->query("SELECT pv.sku, pv.name, pv.price, pr.weight FROM product_values AS pv
                                              INNER JOIN products AS prod ON pv.id = prod.id_product_values
                                              INNER JOIN properties AS pr ON prod.id_properties = pr.id
                                              WHERE prod.id_item_name = 2");
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Book');
        $bookArray = $stmt->fetchAll();
        echo json_encode($bookArray);
    }

    private function findFurniture()
    {
        $stmt = $this->conn->query("SELECT pv.sku, pv.name, pv.price, pr.height, pr.width, pr.length FROM product_values AS pv
                                              INNER JOIN products AS prod ON pv.id = prod.id_product_values
                                              INNER JOIN properties AS pr ON prod.id_properties = pr.id
                                              WHERE prod.id_item_name = 3");
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Furniture');
        $furnitureArray = $stmt->fetchAll();
        echo json_encode($furnitureArray);
    }



}