<?php
//this is the configuration file for the project
    $config = [
        'database' => [
            'dsn' => 'localhost',
            'username' => 'root',
            'password' => '',
            'name' => 'test_scandiweb',
        ],
    ];

    return $config;