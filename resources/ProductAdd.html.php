<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Product Add</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/PageStyling.css">

</head>

<body>
<div class="mainContainer">
    <div class="staticContainer">
        <div class="header">
            <h4>Product Add</h4>
        </div>
    </div>
    <div class="productPages">
        <a href="ProductList"><h4>Product List<h4></a>
    </div>
    <div id="inputContainer">
        <form action="InsertProduct" method="POST">

            <div class="form-group row">
                <label for="sku" class="col-sm-2 col-form-label">SKU</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="sku">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name">
                </div>
            </div>
            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="price">
                </div>
            </div>
            <div class="form-group row">

                    <label for="typeSwitch" class="col-sm-2 col-form-label">Type Switch</label>
                    <div class="col-sm-10">
                        <select class="custom-select mr-sm-2" name="typeSwitch" id="typeSwitch">
                            <option selected value=""></option>
                            <option value="size">Size</option>
                            <option value="weight">Weight</option>
                            <option value="hwl">HxWxL</option>
                        </select>
                    </div>
            </div>
            <div class="form-group row" id="options">

            </div>
            <div class="form-group row" >
                <div class="col-sm-10" id="description"></div>

            </div>
            <button type="submit" class="btn btn-primary mb-2" id="saveButton" >save</button>
            <?php if(isset($succses)) :?>
                <p style="color: darkred"><?=$succses?></p>
            <?php endif; ?>
        </form>
    </div>
</div>
</body>

<script type="text/javascript" src="js/TypeSwitch">

</script>
</html>



