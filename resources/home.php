
<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Home</title>
  </head>
  <body>
    <div style="text-align: center; padding-top: 100px ">
        <div>
            <a href="ProductAdd" class="btn btn-primary btn-lg active" role="button">Add Product</a>
            <a href="ProductList" class="btn btn-primary btn-lg active" role="button">List Products</a>
            <a href="CreateTables" class="btn btn-secondary btn-lg active" role="button">Create Tables</a>
        </div>
        <?php if (isset($createTables)) :?>
            <p style="font-size: large; padding-top: 20px "><?=$createTables?></p>
        <?php endif;?>
        <div class="container pt-5" style="text-align: left">
            <p>Hello! Before products can be added or listed you need to create tables where to store the information.
            Firstly, open the config.php and under the key database add your database info.
            Then pres "Create Tables" and if under the buttons appears text "Tables Created Successfully" you're all set to add products (or list then even if there's
            no products to list). If "Tables already exist!" appears it means that the button has already ben pressed, or if "Tables not created" appears there must
            be some problem with the database connection.</p>
        </div>
    </div>

  </body>
</html>