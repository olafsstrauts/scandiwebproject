<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Product List</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/PageStyling.css">

</head>

<body>
<div class="mainContainer">
    <div class="staticContainer">

        <div class="header">
            <h4>Product List</h4>
        </div>
        <div class="header" id="headerForm">
            <form action="ListProducts" method="post" id="selectForm">
                <select name="options" id="productName">
                    <option value="dvd">DVD</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                </select>
                <input type="submit" value="Select">
            </form>
        </div>
    </div>
    <div class="productPages">
        <a href="ProductAdd"><h4>Product Add<h4></a>
    </div>
        <div class="row pt-3" id="itemsContainer">
            <!--found product go here-->
        </div>

</div>
</body>

<script type="text/javascript" src="js/AjaxPost.js">

</script>
</html>



